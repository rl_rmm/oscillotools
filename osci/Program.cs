﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using BeatsSignalProcessor;

namespace osci
{
    public delegate void BeatsProducerChangedEvent(string newName);
    public delegate void CheckedChangedEvent(bool newState);
    public delegate void ActiveChannelChangedEvent(int activeChannel);
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);


            var Loader = new BeatsProducerLoader();

            var bp = new BeatsProcessor();

            var osciForm = new OsciMainForm(bp);

            int beats = Loader.Producers.Count;

            foreach (var producer in Loader.Producers)
            {
                osciForm.AddBeatsProducer(producer.Key);
            }

            rmm.IBeatsProducer beatsProducer = null;
            
            string bpString = ConfigurationManager.AppSettings["ActiveBeatsProducer"] ?? (beats > 0 ? Loader.Producers.Keys.ElementAt(0) : "");
            string bpAutoStartString = ConfigurationManager.AppSettings["BeatsProducerAutoStart"] ?? true.ToString();
            bool bpAutoStart = bool.Parse(bpAutoStartString);

            if (beats > 0)
            {
                osciForm.CurrentBeatsProducer = bpString;
                osciForm.ActiveSignalChanged += ActiveChannelChanged;

                beatsProducer = Loader.Producers.Values.ElementAt(0);
                osciForm.ChannelsNum = (int)beatsProducer.NumOfChannels;
                osciForm.SamplingRate = beatsProducer.SamplingRate;
                osciForm.SetBeatProducer(beatsProducer);
                beatsProducer.DataReady += (int channelNum, double[] data) =>
                {
                    if (channelNum == ActiveChannel)
                        Task.Run(() => { if (bp.IsWaiting) bp.onNewSignal(data); });
                };

                bool connected = false;
                try
                {
                    connected = beatsProducer.Connect();
                }
                catch(System.Exception e)
                {
                    MessageBox.Show(e.Message, "Ошибка при подключении к источнику сигналов");
                }

                if (bpAutoStart && connected) beatsProducer.Start();
            }
            try
            {
                Application.Run(osciForm);
            }
            finally
            {
                setSetting("ActiveBeatsProducer", osciForm.CurrentBeatsProducer);
                setSetting("BeatsProducerAutoStart", bpAutoStart.ToString());
                configFile.Save(ConfigurationSaveMode.Modified);

                beatsProducer?.Stop();
                beatsProducer?.Disconnect();
            }

            ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
        }

        private static Configuration configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
        private static KeyValueConfigurationCollection settings = configFile.AppSettings.Settings;

        private static void setSetting(string key, string value)
        {
            if (settings[key] == null)
            {
                settings.Add(key, value);
            }
            else
            {
                settings[key].Value = value;
            }
        }

        static int ActiveChannel = 0;
        static void ActiveChannelChanged(int activeChannel) => ActiveChannel = activeChannel;
    }
}
