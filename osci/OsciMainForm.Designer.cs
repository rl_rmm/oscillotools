﻿namespace osci
{
    partial class OsciMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea9 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend9 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series25 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series26 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea10 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend10 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series27 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series28 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series29 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series30 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.lblOk = new System.Windows.Forms.Label();
            this.cbOk = new System.Windows.Forms.CheckBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.rbScaleAutoS = new System.Windows.Forms.RadioButton();
            this.rbScaleFixedS = new System.Windows.Forms.RadioButton();
            this.rbScaleAutoPS = new System.Windows.Forms.RadioButton();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.rbScaleAuto = new System.Windows.Forms.RadioButton();
            this.rbScaleFixed = new System.Windows.Forms.RadioButton();
            this.rbScaleAutoP = new System.Windows.Forms.RadioButton();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnConfig = new System.Windows.Forms.Button();
            this.btnStartStop = new System.Windows.Forms.Button();
            this.cbBeatsProducers = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.numTails = new System.Windows.Forms.NumericUpDown();
            this.numHeads = new System.Windows.Forms.NumericUpDown();
            this.cbTails = new System.Windows.Forms.CheckBox();
            this.cbHeads = new System.Windows.Forms.CheckBox();
            this.cbHamming = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cbActiveChannel = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbPhase = new System.Windows.Forms.CheckBox();
            this.cbAbs = new System.Windows.Forms.CheckBox();
            this.cbReIm = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbAutoUpdate = new System.Windows.Forms.CheckBox();
            this.btnManualUpdate = new System.Windows.Forms.Button();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHeads)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(5, 5);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(1151, 568);
            this.splitContainer1.SplitterDistance = 290;
            this.splitContainer1.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.groupBox6);
            this.panel1.Controls.Add(this.groupBox5);
            this.panel1.Controls.Add(this.groupBox4);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(290, 568);
            this.panel1.TabIndex = 0;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.button1);
            this.groupBox6.Controls.Add(this.lblOk);
            this.groupBox6.Controls.Add(this.cbOk);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox6.Location = new System.Drawing.Point(0, 457);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(290, 56);
            this.groupBox6.TabIndex = 5;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Всё норм?";
            // 
            // lblOk
            // 
            this.lblOk.AutoSize = true;
            this.lblOk.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblOk.Location = new System.Drawing.Point(44, 16);
            this.lblOk.Name = "lblOk";
            this.lblOk.Size = new System.Drawing.Size(35, 13);
            this.lblOk.TabIndex = 1;
            this.lblOk.Text = "label3";
            // 
            // cbOk
            // 
            this.cbOk.AutoSize = true;
            this.cbOk.Dock = System.Windows.Forms.DockStyle.Left;
            this.cbOk.Location = new System.Drawing.Point(3, 16);
            this.cbOk.Name = "cbOk";
            this.cbOk.Size = new System.Drawing.Size(41, 37);
            this.cbOk.TabIndex = 0;
            this.cbOk.Text = "Да";
            this.cbOk.UseVisualStyleBackColor = true;
            this.cbOk.CheckedChanged += new System.EventHandler(this.cbOk_CheckedChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.groupBox8);
            this.groupBox5.Controls.Add(this.groupBox7);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox5.Location = new System.Drawing.Point(0, 346);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(290, 111);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Масштаб";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.rbScaleAutoS);
            this.groupBox8.Controls.Add(this.rbScaleFixedS);
            this.groupBox8.Controls.Add(this.rbScaleAutoPS);
            this.groupBox8.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox8.Location = new System.Drawing.Point(147, 16);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(141, 92);
            this.groupBox8.TabIndex = 4;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Сигнал";
            // 
            // rbScaleAutoS
            // 
            this.rbScaleAutoS.AutoSize = true;
            this.rbScaleAutoS.Location = new System.Drawing.Point(6, 19);
            this.rbScaleAutoS.Name = "rbScaleAutoS";
            this.rbScaleAutoS.Size = new System.Drawing.Size(49, 17);
            this.rbScaleAutoS.TabIndex = 0;
            this.rbScaleAutoS.TabStop = true;
            this.rbScaleAutoS.Text = "Авто";
            this.rbScaleAutoS.UseVisualStyleBackColor = true;
            // 
            // rbScaleFixedS
            // 
            this.rbScaleFixedS.AutoSize = true;
            this.rbScaleFixedS.Location = new System.Drawing.Point(6, 65);
            this.rbScaleFixedS.Name = "rbScaleFixedS";
            this.rbScaleFixedS.Size = new System.Drawing.Size(110, 17);
            this.rbScaleFixedS.TabIndex = 2;
            this.rbScaleFixedS.TabStop = true;
            this.rbScaleFixedS.Text = "Фиксированный";
            this.rbScaleFixedS.UseVisualStyleBackColor = true;
            // 
            // rbScaleAutoPS
            // 
            this.rbScaleAutoPS.AutoSize = true;
            this.rbScaleAutoPS.Location = new System.Drawing.Point(6, 42);
            this.rbScaleAutoPS.Name = "rbScaleAutoPS";
            this.rbScaleAutoPS.Size = new System.Drawing.Size(125, 17);
            this.rbScaleAutoPS.TabIndex = 1;
            this.rbScaleAutoPS.TabStop = true;
            this.rbScaleAutoPS.Text = "Авто на увеличение";
            this.rbScaleAutoPS.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.rbScaleAuto);
            this.groupBox7.Controls.Add(this.rbScaleFixed);
            this.groupBox7.Controls.Add(this.rbScaleAutoP);
            this.groupBox7.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox7.Location = new System.Drawing.Point(3, 16);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(144, 92);
            this.groupBox7.TabIndex = 3;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Спектр";
            // 
            // rbScaleAuto
            // 
            this.rbScaleAuto.AutoSize = true;
            this.rbScaleAuto.Location = new System.Drawing.Point(6, 19);
            this.rbScaleAuto.Name = "rbScaleAuto";
            this.rbScaleAuto.Size = new System.Drawing.Size(49, 17);
            this.rbScaleAuto.TabIndex = 0;
            this.rbScaleAuto.TabStop = true;
            this.rbScaleAuto.Text = "Авто";
            this.rbScaleAuto.UseVisualStyleBackColor = true;
            // 
            // rbScaleFixed
            // 
            this.rbScaleFixed.AutoSize = true;
            this.rbScaleFixed.Location = new System.Drawing.Point(6, 65);
            this.rbScaleFixed.Name = "rbScaleFixed";
            this.rbScaleFixed.Size = new System.Drawing.Size(110, 17);
            this.rbScaleFixed.TabIndex = 2;
            this.rbScaleFixed.TabStop = true;
            this.rbScaleFixed.Text = "Фиксированный";
            this.rbScaleFixed.UseVisualStyleBackColor = true;
            // 
            // rbScaleAutoP
            // 
            this.rbScaleAutoP.AutoSize = true;
            this.rbScaleAutoP.Location = new System.Drawing.Point(6, 42);
            this.rbScaleAutoP.Name = "rbScaleAutoP";
            this.rbScaleAutoP.Size = new System.Drawing.Size(125, 17);
            this.rbScaleAutoP.TabIndex = 1;
            this.rbScaleAutoP.TabStop = true;
            this.rbScaleAutoP.Text = "Авто на увеличение";
            this.rbScaleAutoP.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnConfig);
            this.groupBox4.Controls.Add(this.btnStartStop);
            this.groupBox4.Controls.Add(this.cbBeatsProducers);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox4.Location = new System.Drawing.Point(0, 256);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(290, 90);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Источник";
            // 
            // btnConfig
            // 
            this.btnConfig.Location = new System.Drawing.Point(163, 51);
            this.btnConfig.Name = "btnConfig";
            this.btnConfig.Size = new System.Drawing.Size(121, 23);
            this.btnConfig.TabIndex = 10;
            this.btnConfig.Text = "Настроить";
            this.btnConfig.UseVisualStyleBackColor = true;
            this.btnConfig.Click += new System.EventHandler(this.btnConfig_Click);
            // 
            // btnStartStop
            // 
            this.btnStartStop.Location = new System.Drawing.Point(12, 51);
            this.btnStartStop.Name = "btnStartStop";
            this.btnStartStop.Size = new System.Drawing.Size(75, 23);
            this.btnStartStop.TabIndex = 8;
            this.btnStartStop.Text = "Старт";
            this.btnStartStop.UseVisualStyleBackColor = true;
            this.btnStartStop.Click += new System.EventHandler(this.button2_Click);
            // 
            // cbBeatsProducers
            // 
            this.cbBeatsProducers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbBeatsProducers.FormattingEnabled = true;
            this.cbBeatsProducers.Location = new System.Drawing.Point(163, 24);
            this.cbBeatsProducers.Name = "cbBeatsProducers";
            this.cbBeatsProducers.Size = new System.Drawing.Size(121, 21);
            this.cbBeatsProducers.TabIndex = 7;
            this.cbBeatsProducers.SelectedIndexChanged += new System.EventHandler(this.cbBeatsProducers_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Активный источник:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.numTails);
            this.groupBox3.Controls.Add(this.numHeads);
            this.groupBox3.Controls.Add(this.cbTails);
            this.groupBox3.Controls.Add(this.cbHeads);
            this.groupBox3.Controls.Add(this.cbHamming);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Location = new System.Drawing.Point(0, 156);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(290, 100);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Обработка";
            // 
            // numTails
            // 
            this.numTails.Location = new System.Drawing.Point(83, 67);
            this.numTails.Name = "numTails";
            this.numTails.Size = new System.Drawing.Size(120, 20);
            this.numTails.TabIndex = 4;
            // 
            // numHeads
            // 
            this.numHeads.Location = new System.Drawing.Point(83, 43);
            this.numHeads.Name = "numHeads";
            this.numHeads.Size = new System.Drawing.Size(120, 20);
            this.numHeads.TabIndex = 3;
            this.numHeads.ValueChanged += new System.EventHandler(this.numHeads_ValueChanged);
            // 
            // cbTails
            // 
            this.cbTails.AutoSize = true;
            this.cbTails.Location = new System.Drawing.Point(13, 68);
            this.cbTails.Name = "cbTails";
            this.cbTails.Size = new System.Drawing.Size(64, 17);
            this.cbTails.TabIndex = 2;
            this.cbTails.Text = "Хвосты";
            this.cbTails.UseVisualStyleBackColor = true;
            // 
            // cbHeads
            // 
            this.cbHeads.AutoSize = true;
            this.cbHeads.Location = new System.Drawing.Point(13, 44);
            this.cbHeads.Name = "cbHeads";
            this.cbHeads.Size = new System.Drawing.Size(64, 17);
            this.cbHeads.TabIndex = 1;
            this.cbHeads.Text = "Головы";
            this.cbHeads.UseVisualStyleBackColor = true;
            // 
            // cbHamming
            // 
            this.cbHamming.AutoSize = true;
            this.cbHamming.Location = new System.Drawing.Point(13, 20);
            this.cbHamming.Name = "cbHamming";
            this.cbHamming.Size = new System.Drawing.Size(105, 17);
            this.cbHamming.TabIndex = 0;
            this.cbHamming.Text = "Окно хэмминга";
            this.cbHamming.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cbActiveChannel);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.cbPhase);
            this.groupBox2.Controls.Add(this.cbAbs);
            this.groupBox2.Controls.Add(this.cbReIm);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(0, 56);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(290, 100);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Спектр";
            // 
            // cbActiveChannel
            // 
            this.cbActiveChannel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbActiveChannel.FormattingEnabled = true;
            this.cbActiveChannel.Location = new System.Drawing.Point(150, 42);
            this.cbActiveChannel.Name = "cbActiveChannel";
            this.cbActiveChannel.Size = new System.Drawing.Size(121, 21);
            this.cbActiveChannel.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(147, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Активный сигнал:";
            // 
            // cbPhase
            // 
            this.cbPhase.AutoSize = true;
            this.cbPhase.Location = new System.Drawing.Point(12, 65);
            this.cbPhase.Name = "cbPhase";
            this.cbPhase.Size = new System.Drawing.Size(75, 17);
            this.cbPhase.TabIndex = 4;
            this.cbPhase.Text = "Фазовый";
            this.cbPhase.UseVisualStyleBackColor = true;
            this.cbPhase.CheckedChanged += new System.EventHandler(this.cbPhase_CheckedChanged);
            // 
            // cbAbs
            // 
            this.cbAbs.AutoSize = true;
            this.cbAbs.Location = new System.Drawing.Point(12, 20);
            this.cbAbs.Name = "cbAbs";
            this.cbAbs.Size = new System.Drawing.Size(95, 17);
            this.cbAbs.TabIndex = 2;
            this.cbAbs.Text = "Амплитудный";
            this.cbAbs.UseVisualStyleBackColor = true;
            this.cbAbs.CheckedChanged += new System.EventHandler(this.cbAbs_CheckedChanged);
            // 
            // cbReIm
            // 
            this.cbReIm.AutoSize = true;
            this.cbReIm.Location = new System.Drawing.Point(12, 42);
            this.cbReIm.Name = "cbReIm";
            this.cbReIm.Size = new System.Drawing.Size(97, 17);
            this.cbReIm.TabIndex = 3;
            this.cbReIm.Text = "Комплексный";
            this.cbReIm.UseVisualStyleBackColor = true;
            this.cbReIm.CheckedChanged += new System.EventHandler(this.cbReIm_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbAutoUpdate);
            this.groupBox1.Controls.Add(this.btnManualUpdate);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(290, 56);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Графики";
            // 
            // cbAutoUpdate
            // 
            this.cbAutoUpdate.AutoSize = true;
            this.cbAutoUpdate.Dock = System.Windows.Forms.DockStyle.Left;
            this.cbAutoUpdate.Location = new System.Drawing.Point(3, 16);
            this.cbAutoUpdate.Name = "cbAutoUpdate";
            this.cbAutoUpdate.Size = new System.Drawing.Size(110, 37);
            this.cbAutoUpdate.TabIndex = 1;
            this.cbAutoUpdate.Text = "Автообновление";
            this.cbAutoUpdate.UseVisualStyleBackColor = true;
            this.cbAutoUpdate.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // btnManualUpdate
            // 
            this.btnManualUpdate.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnManualUpdate.Location = new System.Drawing.Point(185, 16);
            this.btnManualUpdate.Name = "btnManualUpdate";
            this.btnManualUpdate.Size = new System.Drawing.Size(102, 37);
            this.btnManualUpdate.TabIndex = 0;
            this.btnManualUpdate.Text = "Обновить сейчас";
            this.btnManualUpdate.UseVisualStyleBackColor = true;
            this.btnManualUpdate.Click += new System.EventHandler(this.button1_Click);
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.chart1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.chart2);
            this.splitContainer2.Size = new System.Drawing.Size(857, 568);
            this.splitContainer2.SplitterDistance = 273;
            this.splitContainer2.TabIndex = 0;
            // 
            // chart1
            // 
            chartArea9.CursorX.AutoScroll = false;
            chartArea9.CursorX.IsUserEnabled = true;
            chartArea9.CursorX.IsUserSelectionEnabled = true;
            chartArea9.CursorY.AutoScroll = false;
            chartArea9.CursorY.Interval = 0.001D;
            chartArea9.CursorY.IsUserEnabled = true;
            chartArea9.CursorY.IsUserSelectionEnabled = true;
            chartArea9.Name = "ChartArea1";
            chartArea9.Visible = false;
            this.chart1.ChartAreas.Add(chartArea9);
            this.chart1.Dock = System.Windows.Forms.DockStyle.Fill;
            legend9.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom;
            legend9.Name = "Legend1";
            this.chart1.Legends.Add(legend9);
            this.chart1.Location = new System.Drawing.Point(0, 0);
            this.chart1.Name = "chart1";
            series25.ChartArea = "ChartArea1";
            series25.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            series25.Enabled = false;
            series25.Legend = "Legend1";
            series25.LegendText = "Канал 1";
            series25.Name = "chan1";
            series26.ChartArea = "ChartArea1";
            series26.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            series26.Enabled = false;
            series26.Legend = "Legend1";
            series26.LegendText = "Канал 2";
            series26.Name = "chan2";
            this.chart1.Series.Add(series25);
            this.chart1.Series.Add(series26);
            this.chart1.Size = new System.Drawing.Size(857, 273);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            // 
            // chart2
            // 
            chartArea10.CursorX.IsUserEnabled = true;
            chartArea10.CursorX.IsUserSelectionEnabled = true;
            chartArea10.CursorY.Interval = 0.001D;
            chartArea10.CursorY.IsUserEnabled = true;
            chartArea10.CursorY.IsUserSelectionEnabled = true;
            chartArea10.Name = "ChartArea1";
            chartArea10.Visible = false;
            this.chart2.ChartAreas.Add(chartArea10);
            this.chart2.Dock = System.Windows.Forms.DockStyle.Fill;
            legend10.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom;
            legend10.Name = "Legend1";
            this.chart2.Legends.Add(legend10);
            this.chart2.Location = new System.Drawing.Point(0, 0);
            this.chart2.Name = "chart2";
            series27.ChartArea = "ChartArea1";
            series27.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            series27.Enabled = false;
            series27.Legend = "Legend1";
            series27.Name = "abs";
            series28.ChartArea = "ChartArea1";
            series28.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            series28.Enabled = false;
            series28.Legend = "Legend1";
            series28.Name = "re";
            series29.ChartArea = "ChartArea1";
            series29.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            series29.Enabled = false;
            series29.Legend = "Legend1";
            series29.Name = "im";
            series30.ChartArea = "ChartArea1";
            series30.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            series30.Enabled = false;
            series30.Legend = "Legend1";
            series30.Name = "phi";
            this.chart2.Series.Add(series27);
            this.chart2.Series.Add(series28);
            this.chart2.Series.Add(series29);
            this.chart2.Series.Add(series30);
            this.chart2.Size = new System.Drawing.Size(857, 291);
            this.chart2.TabIndex = 0;
            this.chart2.Text = "chart2";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(251, 16);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(33, 31);
            this.button1.TabIndex = 11;
            this.button1.Text = "⏚";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // OsciMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1161, 578);
            this.Controls.Add(this.splitContainer1);
            this.Name = "OsciMainForm";
            this.Padding = new System.Windows.Forms.Padding(5);
            this.Text = "OsciMainForm";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHeads)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnManualUpdate;
        private System.Windows.Forms.CheckBox cbAutoUpdate;
        private System.Windows.Forms.ComboBox cbActiveChannel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox cbPhase;
        private System.Windows.Forms.CheckBox cbAbs;
        private System.Windows.Forms.CheckBox cbReIm;
        private System.Windows.Forms.CheckBox cbTails;
        private System.Windows.Forms.CheckBox cbHeads;
        private System.Windows.Forms.CheckBox cbHamming;
        private System.Windows.Forms.ComboBox cbBeatsProducers;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnStartStop;
        private System.Windows.Forms.Button btnConfig;
        private System.Windows.Forms.NumericUpDown numTails;
        private System.Windows.Forms.NumericUpDown numHeads;
        private System.Windows.Forms.RadioButton rbScaleAutoP;
        private System.Windows.Forms.RadioButton rbScaleAuto;
        private System.Windows.Forms.RadioButton rbScaleFixed;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.RadioButton rbScaleAutoS;
        private System.Windows.Forms.RadioButton rbScaleFixedS;
        private System.Windows.Forms.RadioButton rbScaleAutoPS;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.CheckBox cbOk;
        private System.Windows.Forms.Label lblOk;
        private System.Windows.Forms.Button button1;
    }
}

