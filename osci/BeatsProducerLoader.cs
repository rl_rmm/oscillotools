﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace osci
{
    using BeatsProducersMap = Dictionary<string, rmm.IBeatsProducer>;
    class BeatsProducerLoader
    {

        public BeatsProducersMap Producers;

        public BeatsProducerLoader()
        {
            Producers = GetAvailableProducers();
        }

        private BeatsProducersMap GetAvailableProducers()
        {
            string Cwd = AppDomain.CurrentDomain.BaseDirectory;
            string[] Files = System.IO.Directory.GetFiles(Cwd, "*.dll");
            var Producers = new Dictionary<string, rmm.IBeatsProducer>();
            foreach (var File in Files)
            {
                try
                {
                    var asm = Assembly.LoadFile(File);
                    Type[] types = asm.GetTypes();

                    for (int i = 0; i < types.Length; i++)
                    {
                        Type type = asm.GetType(types[i].FullName);
                        if (type.GetInterface("rmm.IBeatsProducer") != null)
                        {
                            rmm.IBeatsProducer obj = (rmm.IBeatsProducer)Activator.CreateInstance(type);
                            if (obj != null)
                                Producers[type.FullName] = obj;
                        }
                    }
                }
                catch
                { }
                finally
                {

                }
            }
            return Producers;
        }

    }
}
