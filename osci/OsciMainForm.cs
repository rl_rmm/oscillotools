﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using BeatsSignalProcessor;

namespace osci
{
    public partial class OsciMainForm : Form
    {
        public OsciMainForm(BeatsProcessor beatsProcessor)
        {
            InitializeComponent();

            chart2.Customize += (s, e) => chart2.Update();

            _beatsProcessor = beatsProcessor;
            refreshTimer = new Timer();
            refreshTimer.Interval = 20;
            refreshTimer.Tick += (s, e) =>
            {
                if (newSignalReceived)
                {
                    try
                    {
                        UpdateCharts();
                    }
                    catch
                    {
                        recreateChart1();
                        recreateChart2();
                    }
                }
                newSignalReceived = false;
            };

            failTimer = new Timer();
            failTimer.Interval = 2000;
            failTimer.Tick += (s, e) => { failNow = true; };

            cbHamming.CheckedChanged += (s, e) => { _beatsProcessor.HammingEnabled = cbHamming.Checked; };
            cbHeads.CheckedChanged += (s, e) => { _beatsProcessor.TrimHeadEnabled = cbHeads.Checked; };
            cbTails.CheckedChanged += (s, e) => { _beatsProcessor.TrimTailEnabled = cbTails.Checked; };
            numHeads.ValueChanged += (s, e) => { _beatsProcessor.TrimHeadSize = (int)numHeads.Value; };
            numTails.ValueChanged += (s, e) => { _beatsProcessor.TrimTailSize = (int)numTails.Value; };
            cbActiveChannel.SelectedIndexChanged += (s, _) => ActiveSignalChanged?.Invoke(((ComboBox)s).SelectedIndex);

            rbScaleAutoP.Checked = true;
            rbScaleAutoPS.Checked = true;

            cbAutoUpdate.Checked = true;
            cbAbs.Checked = true;
            cbOk.Checked = true;
        }
        public void AddBeatsProducer(string producerName)
        {
            cbBeatsProducers.Items.Add(producerName);
        }

        private rmm.IBeatsProducer m_beatsProducer = null;
        public void SetBeatProducer(rmm.IBeatsProducer beatsProducer)
        {
            beatsProducer.DataReady += this.ReceiveSignal;
            m_beatsProducer = beatsProducer;
        }

        public double SamplingRate;

        public string CurrentBeatsProducer
        {
            get => cbBeatsProducers.SelectedItem.ToString();
            set
            {
                if (cbBeatsProducers.Items.Contains(value))
                    cbBeatsProducers.SelectedItem = value;
            }
        }

        public int ActiveChannel
        {
            get => cbActiveChannel.SelectedIndex;
            set
            {
                if (cbActiveChannel.Items.Count > value)
                    cbActiveChannel.SelectedIndex = value;
            }
        }

        public int ChannelsNum
        {
            get => cbActiveChannel.Items.Count;
            set
            {
                cbActiveChannel.Items.Clear();
                for (int i = 0; i < value; i++)
                {
                    cbActiveChannel.Items.Add($"Канал {i + 1}");
                }
                ActiveChannel = 0;
            }
        }

        public event BeatsProducerChangedEvent CurrentBeatsProducerChanged;
        public event ActiveChannelChangedEvent ActiveSignalChanged;

        private double[][] signals = new double[2][];
        private double[] abs;
        private double[] re;
        private double[] im;
        private double[] ph;
        private bool newSignalReceived = false;
        private bool showAbs { get => cbAbs.Checked && abs != null; }
        private bool showReIm { get => cbReIm.Checked && re != null && im != null; }
        private bool showPhi { get => cbPhase.Checked && ph != null; }
        private bool showChart2 { get => showAbs || showReIm || showPhi; }
        private double maxY = 0;
        private double maxYs = 0;
        private Timer refreshTimer;
        private Timer failTimer;
        private bool failNow = false;
        private static readonly double minDecimal = 1e-25D;
        private double D2d(double d)
        {
            if (d > 0)
                d = Math.Min(d + minDecimal, (double)Decimal.MaxValue * 0.1 - minDecimal);
            else
                d = Math.Max(d - minDecimal, (double)Decimal.MinValue * 0.1 + minDecimal);

            return d;
        }
        private void UpdateCharts()
        {
            double lastMaxYs = maxYs;
            if (!rbScaleAutoPS.Checked)
                maxYs = 0;
            chart1.ChartAreas["ChartArea1"].Visible = true;
            chart1.Series["chan1"].Enabled = true;
            chart1.Series["chan1"].Points.Clear();
            chart1.Series["chan2"].Enabled = true;
            chart1.Series["chan2"].Points.Clear();

            decimal timeStep = (decimal)(1000D / SamplingRate);

            int i;
            for (i = 0; i < signals[0].Length; i++)
            {
                double point = D2d(signals[0][i]);
                chart1.Series["chan1"].Points.AddXY(timeStep * i++, point);
                maxYs = Math.Max(maxYs, point);
            }

            for (i = 0; i < signals[1].Length; i++)
            {
                double point = D2d(signals[1][i]);
                chart1.Series["chan2"].Points.AddXY(timeStep * i++, point);
                maxYs = Math.Max(maxYs, point);
            }

            double dMaxYs = maxYs - lastMaxYs;
            if (lastMaxYs != 0 && dMaxYs > 0.1 * lastMaxYs) maxYs = 1.1 * lastMaxYs;

            maxYs = (int)(maxYs * 1000D) / 1000D;
            double minYs = (int)(-maxYs * 1000D) / 1000D;

            if (minYs > maxYs) minYs = maxYs - 1;

            if (!rbScaleFixedS.Checked)
            {
                chart1.ChartAreas["ChartArea1"].AxisY.Maximum = Math.Max(maxYs, 0.001);
                chart1.ChartAreas["ChartArea1"].AxisY.Minimum = minYs;
            }

            chart1.ChartAreas["ChartArea1"].AxisX.Maximum = 1000 * signals[0].Length / SamplingRate;
            chart1.ChartAreas["ChartArea1"].AxisX.Minimum = 0;
            chart1.ChartAreas["ChartArea1"].AxisX.Interval = 100 * signals[0].Length / SamplingRate;

            chart1.Update();

            if (showChart2)
            {
                double lastMaxY = maxY;
                double minY = 0;
                if (!rbScaleAutoP.Checked)
                    maxY = 0;
                int step = _beatsProcessor.SpectrSize / chart2.Width;
                decimal phiStep = (decimal)(SamplingRate / _beatsProcessor.SpectrSize);

                chart2.ChartAreas["ChartArea1"].Visible = true;
                chart2.Series["phi"].Points.Clear();
                chart2.Series["abs"].Points.Clear();
                chart2.Series["re"].Points.Clear();
                chart2.Series["im"].Points.Clear();

                if (showAbs && abs != null)
                {
                    for (i = 0; i < abs.Length;)
                    {
                        double point = D2d(abs[i]);
                        chart2.Series["abs"].Points.AddXY(i * phiStep, point);
                        maxY = Math.Max(maxY, point);
                        i += step;
                    }
                }

                if (showReIm)
                {
                    for (i = 0; i < re.Length;)
                    {
                        double point = D2d(re[i]);
                        maxY = Math.Max(maxY, point);
                        chart2.Series["re"].Points.AddXY(i * phiStep, point);
                        point = D2d(im[i]);
                        chart2.Series["im"].Points.AddXY(i * phiStep, point);
                        i += step;
                    }
                }

                if (showPhi && ph != null)
                {
                    for (i = 0; i < ph.Length;)
                    {
                        double point = D2d(ph[i] / 2D / Math.PI * maxY);
                        chart2.Series["phi"].Points.AddXY(i * phiStep, point);
                        i += step;
                    }
                }

                double dMaxY = maxY - lastMaxY;
                if (lastMaxY != 0 && dMaxY > 0.1 * lastMaxY) maxY = 1.1 * lastMaxY;

                maxY = Math.Floor(maxY * 1000D) / 1000D;
                minY = (showPhi || showReIm) ? (-maxY) : 0;

                if (!rbScaleFixed.Checked)
                {
                    chart2.ChartAreas["ChartArea1"].AxisY.Maximum = Math.Max(maxY, 0.001);
                    chart2.ChartAreas["ChartArea1"].AxisY.Minimum = Math.Min(minY, -0.001); ;
                }

                chart2.ChartAreas["ChartArea1"].AxisX.Maximum = SamplingRate / 2D;
                chart2.ChartAreas["ChartArea1"].AxisX.Minimum = 0;
                chart2.ChartAreas["ChartArea1"].AxisX.Interval = SamplingRate / 20D;

                chart2.Update();
            }
        }
        public void ReceiveSignal(int channelNum, double[] signal)
        {
            signals[channelNum] = signal;
            numHeads.Maximum = signal.Length - 1;
            numTails.Maximum = signal.Length - 1;
            newSignalReceived = true;
        }
        public void ReceiveAbs(double[] signal)
        {
            abs = signal;
        }
        public void ReceiveRe(double[] signal)
        {
            re = signal;
        }
        public void ReceiveIm(double[] signal)
        {
            im = signal;
        }
        public void ReceivePhase(double[] signal)
        {
            ph = signal;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            UpdateCharts();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            refreshTimer.Enabled = cbAutoUpdate.Checked;
            failTimer.Enabled = cbAutoUpdate.Checked;
            btnManualUpdate.Enabled = !cbAutoUpdate.Checked;
        }

        private void cbBeatsProducers_SelectedIndexChanged(object sender, EventArgs e)
        {
            CurrentBeatsProducerChanged?.Invoke(cbBeatsProducers.SelectedItem.ToString());
        }

        private BeatsProcessor _beatsProcessor;

        private void cbAbs_CheckedChanged(object sender, EventArgs e)
        {
            chart2.Series["abs"].Enabled = cbAbs.Checked;
            if (cbAbs.Checked)
                _beatsProcessor.AbsReady += this.ReceiveAbs;
            else
                _beatsProcessor.AbsReady -= this.ReceiveAbs;
        }

        private void cbReIm_CheckedChanged(object sender, EventArgs e)
        {
            chart2.Series["re"].Enabled = cbReIm.Checked;
            chart2.Series["im"].Enabled = cbReIm.Checked;
            if (cbReIm.Checked)
            {
                _beatsProcessor.ReReady += this.ReceiveRe;
                _beatsProcessor.ImReady += this.ReceiveIm;
            }
            else
            {
                _beatsProcessor.ReReady -= this.ReceiveRe;
                _beatsProcessor.ImReady -= this.ReceiveIm;
            }
        }

        private void cbPhase_CheckedChanged(object sender, EventArgs e)
        {
            chart2.Series["phi"].Enabled = cbPhase.Checked;
            if (cbPhase.Checked)
                _beatsProcessor.PhaseReady += this.ReceivePhase;
            else
                _beatsProcessor.PhaseReady -= this.ReceivePhase;
        }

        private void numHeads_ValueChanged(object sender, EventArgs e)
        {

        }

        private void recreateChart1()
        {
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            chartArea1.CursorX.AutoScroll = false;
            chartArea1.CursorX.IsUserEnabled = true;
            chartArea1.CursorX.IsUserSelectionEnabled = true;
            chartArea1.CursorY.AutoScroll = false;
            chartArea1.CursorY.Interval = 0.001D;
            chartArea1.CursorY.IsUserEnabled = true;
            chartArea1.CursorY.IsUserSelectionEnabled = true;
            chartArea1.Name = "ChartArea1";
            chartArea1.Visible = false;
            this.chart1.ChartAreas.Add(chartArea1);
            this.chart1.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom;
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(0, 0);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            series1.Enabled = false;
            series1.Legend = "Legend1";
            series1.LegendText = "Канал 1";
            series1.Name = "chan1";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            series2.Enabled = false;
            series2.Legend = "Legend1";
            series2.LegendText = "Канал 2";
            series2.Name = "chan2";
            this.chart1.Series.Add(series1);
            this.chart1.Series.Add(series2);
            this.chart1.Size = new System.Drawing.Size(779, 256);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";

            this.splitContainer2.Panel1.Controls.Clear();
            this.splitContainer2.Panel1.Controls.Add(chart1);

            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
        }

        private void recreateChart2()
        {
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();

            chartArea2.CursorX.IsUserEnabled = true;
            chartArea2.CursorX.IsUserSelectionEnabled = true;
            chartArea2.CursorY.Interval = 0.001D;
            chartArea2.CursorY.IsUserEnabled = true;
            chartArea2.CursorY.IsUserSelectionEnabled = true;
            chartArea2.Name = "ChartArea1";
            chartArea2.Visible = showChart2;
            this.chart2.ChartAreas.Add(chartArea2);
            this.chart2.Dock = System.Windows.Forms.DockStyle.Fill;
            legend2.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom;
            legend2.Name = "Legend1";
            this.chart2.Legends.Add(legend2);
            this.chart2.Location = new System.Drawing.Point(0, 0);
            this.chart2.Name = "chart2";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            series3.Enabled = showAbs;
            series3.Legend = "Legend1";
            series3.Name = "abs";
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            series4.Enabled = showReIm;
            series4.Legend = "Legend1";
            series4.Name = "re";
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            series5.Enabled = showReIm;
            series5.Legend = "Legend1";
            series5.Name = "im";
            series6.ChartArea = "ChartArea1";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            series6.Enabled = showPhi;
            series6.Legend = "Legend1";
            series6.Name = "phi";
            this.chart2.Series.Add(series3);
            this.chart2.Series.Add(series4);
            this.chart2.Series.Add(series5);
            this.chart2.Series.Add(series6);
            this.chart2.Size = new System.Drawing.Size(779, 271);
            this.chart2.TabIndex = 0;
            this.chart2.Text = "chart2";

            this.splitContainer2.Panel2.Controls.Clear();
            this.splitContainer2.Panel2.Controls.Add(this.chart2);

            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
        }

        private void cbOk_CheckedChanged(object sender, EventArgs e)
        {
            lblOk.Text = cbOk.Checked ? ":)" : ":(";
        }

        private bool m_producerStarted = false;
        private void button2_Click(object sender, EventArgs e)
        {
            if (m_beatsProducer == null)
                return;
            if (m_producerStarted)
            {
                m_producerStarted = !m_beatsProducer.Stop();
            }
            else
            {
                m_producerStarted = m_beatsProducer.Start();
            }
            btnStartStop.Text = m_producerStarted ? "Стоп" : "Старт";
        }

        private void btnConfig_Click(object sender, EventArgs e)
        {
            if (m_beatsProducer == null)
                return;
            _ = m_beatsProducer.ShowConfigurationForm();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            using (var image = new Bitmap(Properties.Resources.ground))
            using (var gndForm = new Form
            {
                Width = image.Width,
                Height = image.Height,
                BackgroundImage = image
            })
            {
                gndForm.Shown += (s, _) => { System.Threading.Thread.Sleep(333); gndForm.Close(); };
                gndForm.ShowDialog();
            }
        }
    }
}
