﻿using static System.Math;
using FFTW.NET;

namespace BeatsSignalProcessor
{
    public delegate void ProcResult(double[] data);

    // Получаем сигнал, готовим, откидываем четыре три массива на спектр.
    public class BeatsProcessor
    {
        private FftwArrayComplex fftwInput;
        private FftwArrayComplex fftwOutput;
        private FftwPlanC2C fftw;
        private int fftwSize;
        private int lastSignalLength = 0;
        private int lastHammingLength = 0;

        private double[] hamming;

        public bool TrimHeadEnabled;
        public int TrimHeadSize;

        public bool TrimTailEnabled;
        public int TrimTailSize;

        public int SpectrSize { get => fftwSize; }

        public bool HammingEnabled;

        public event ProcResult AbsReady;
        public event ProcResult ReReady;
        public event ProcResult ImReady;
        public event ProcResult PhaseReady;

        private bool _waiting = false;
        public bool IsWaiting { get => _waiting; }

        public BeatsProcessor(int fftwSize = 65536 )
        {
            this.fftwSize = fftwSize;
            fftwInput = new FftwArrayComplex(fftwSize);
            fftwOutput = new FftwArrayComplex(fftwSize);

            for (int i = 0; i < fftwSize; i++)
            {
                fftwInput[i] = 0;
                fftwOutput[i] = 0;
            }

            fftw = FftwPlanC2C.Create(fftwInput, fftwOutput, DftDirection.Forwards);

            hamming = null;

            _waiting = true;
        }

        public void onNewSignal(double[] signal)
        {
            if (!_waiting) 
                return;
            if ((AbsReady == null) && (ReReady == null) && (ImReady == null) && (PhaseReady == null))
                return;

            _waiting = false;

            int hammingLength = signal.Length;
            int hammingBegin = 0;
            if (TrimHeadEnabled)
            {
                hammingLength -= TrimHeadSize;
                hammingBegin += TrimHeadSize;
                for (int i = 0; i < TrimHeadSize; i++)
                    signal[i] = 0;
            }

            if (TrimTailEnabled)
            {
                hammingLength -= TrimTailSize;
                for (int i = signal.Length - TrimTailSize - 1; i < signal.Length; i++)
                    signal[i] = 0;
            }

            if (lastSignalLength > signal.Length)
            {
                for (int i = signal.Length; i < lastSignalLength; i++)
                    fftwInput[i] = 0;
                lastSignalLength = signal.Length;
            }

            if (hammingLength <= 0)
                return;

            if (HammingEnabled)
            {
                if (hammingLength != lastHammingLength)
                {
                    lastHammingLength = hammingLength;
                    double a = 0.54;
                    double b = 0.46;
                    hamming = new double[hammingLength];
                    for (int i = 0; i < hammingLength; i++)
                        hamming[i] = a - (b * Cos(2 * PI * i / (hammingLength - 1)));
                }

                for (int i = 0; i < hammingLength; i++)
                {
                    signal[i + hammingBegin] *= hamming[i];
                }
            }

            for (int i = 0; i < signal.Length; i++)
                fftwInput[i] = signal[i];

            fftw.Execute();

            int sendLength = fftwOutput.Length / 2;

            if (AbsReady != null)
            {
                double[] abs = new double[sendLength];
                for (int i = 0; i < sendLength; i++)
                {
                    abs[i] = fftwOutput[i].Magnitude;
                }
                AbsReady?.Invoke(abs);
            }

            if (ReReady != null)
            {
                double[] re = new double[sendLength];
                for (int i = 0; i < sendLength; i++)
                {
                    re[i] = fftwOutput[i].Real;
                }
                ReReady?.Invoke(re);
            }

            if (ImReady != null)
            {
                double[] im = new double[sendLength];
                for (int i = 0; i < sendLength; i++)
                {
                    im[i] = fftwOutput[i].Imaginary;
                }
                ImReady?.Invoke(im);
            }

            if (PhaseReady != null)
            {
                double[] ph = new double[sendLength];
                for (int i = 0; i < sendLength; i++)
                {
                    ph[i] = fftwOutput[i].Phase;
                }
                PhaseReady?.Invoke(ph);
            }

            _waiting = true;
        }
    }
}
